import org.apache.hadoop.mapred.*;
import com.amd.aparapi.Kernel;
import com.amd.aparapi.Range;

import java.io.IOException;
import java.util.*;

public abstract class MapperKernel2FloatToBoolean<K1, V1, K2, V2>
extends MapperKernel<K1, V1, K2, V2, Tuple2<Float, Float>, Boolean>{

	public abstract List<Tuple2<Float, Float> > preprocess(K1 keyIn, V1 valueIn);
	public abstract List<Tuple2<K2, V2> > postprocess(List<Boolean> gpuOut) ;
	public abstract boolean gpu(float arg1, float arg2) ;
	public void postprocess(List<Boolean> gpuOut, 
		OutputCollector<K2, V2> output) {

	}

	public void map(K1 key, V1 value, OutputCollector<K2, V2> output, Reporter reporter)
	throws IOException {
		List<Tuple2<Float, Float> > list = preprocess(key, value);
		int size = list.size();
		final float array1[] = new float[size];
		final float array2[] = new float[size];
		int idx = 0;
		for(Tuple2<Float, Float> tuple : list) {
			array1[idx] = tuple.first();
			array2[idx] = tuple.second();
			idx++;
		}

		final boolean[] result = new boolean[size];
		Kernel kernel = new Kernel(){
			@Override public void run() {
				int gid = getGlobalId();
				result[gid] = gpu(array1[gid], array2[gid]);
			}
		};
		kernel.execute(Range.create(size));
		

		ArrayList<Boolean> goList = new ArrayList<Boolean>(size);
		for(int i = 0; i < size; ++i)
			goList.add(result[i]);

		List<Tuple2<K2, V2> > outList = postprocess(goList);
		for(Tuple2<K2, V2> tuple : outList) {
			output.collect(tuple.first(), tuple.second());
		}

	}

}