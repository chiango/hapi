public class Tuple2<L, R> {
	private L l;
	private R r;

	public Tuple2(L l, R r) {
		this.l = l;
		this.r = r;
	}
	public L first() {
		return l;
	}
	public R second() {
		return r;
	}
}