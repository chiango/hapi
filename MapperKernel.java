import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.*;

/**
*	K1: KeyIn V1: ValueIn
*	K2: KeyOut V2: ValueOut
*	GI: GpuIn GO: GpuOut
*/
public abstract class MapperKernel<K1, V1, K2, V2, GI, GO> implements Mapper<K1, V1, K2, V2> {

	public abstract List<GI> preprocess(K1 keyIn, V1 valueIn) ;
	public abstract List<Tuple2<K2, V2> > postprocess(List<GO> gpuOut) ;
	/** Default implementation that does nothing. */
	public void close() throws IOException {
	}

	/** Default implementation that does nothing. */
	public void configure(JobConf job) {
	}
}